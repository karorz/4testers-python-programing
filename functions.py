def square_given_number(input_number):
    return input_number ** 2

def void_of_cuboid(side_a, side_b, side_c):
    return side_a * side_b * side_c

def celcius_to_farenheith(celcius):
    return celcius * 1.8 + 32

if __name__ == '__main__':

    zero_sqared = square_given_number(0)
    sixteen_sqared = square_given_number(16)
    float_sqared = square_given_number(2.55)
    print("Square of 0 is:", zero_sqared)
    print("Square of 16 is:", sixteen_sqared)
    print("Square of 2.55 is:", float_sqared)

    cuboid = void_of_cuboid(3, 5, 7)
    print("Void of cuboid 3x5x7:", cuboid)

    temperature_farenheith = celcius_to_farenheith(25)
    print("25 Celcius is Ferenheith:", temperature_farenheith)