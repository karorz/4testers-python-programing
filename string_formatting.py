if __name__ == '__main__':
    first_name = "Karol"
    last_name = "Orzechowski"
    email = first_name.lower() + "." + last_name.lower() + "@4testers.pl"
    print(email)

    email_formatted = f"{first_name.lower()}.{last_name.lower()}@4testers.pl"
    print(email_formatted)